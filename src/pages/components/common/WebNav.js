import Link from 'next/link';
import React, {useEffect, useState} from 'react';
import { useRouter } from "next/router";
function WebNav() {
  const [menuIcon, setMenuIcon] = useState();
  const [showScroll, setShowScroll] = useState(false)
  const checkScrollTop = () => {    
     if (!showScroll && window.pageYOffset > 70){
        setShowScroll(true)    
     } else if (showScroll && window.pageYOffset <= 70){
        setShowScroll(false)    
     }  
  };
  useEffect(()=>{
    window.addEventListener('scroll', checkScrollTop)

  })
  const router = useRouter();
 
  return (
    <>
     <nav className={`navbar navbar-expand-lg fixed-top ${showScroll ? "slick-nav":""} ${menuIcon ? "mobile-nav" : ""}`}>
              <div className="container">
                
                  <Link className="navbar-brand" href="/">Inchbrick</Link>
                  <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
                      <li className={`nav-item ${router.pathname == "/" ? "active" : ""}`}>
                          <Link className="nav-link" href="/">Home</Link>
                      </li>
                      <li className={`nav-item ${router.pathname == "/about" ? "active" : ""}`}>
                          <Link className="nav-link" href="about">About</Link>
                      </li>
                      <li className={`nav-item ${router.pathname == "/contact" ? "active" : ""}`}>
                          <Link className="nav-link" href="contact">Contact</Link>
                      </li>
                      <li className={`nav-item ${router.pathname == "/blogs" ? "active" : ""}`}>
                          <Link className="nav-link" href="blogs">Blogs</Link>
                      </li>
                      <li className={`nav-item ${router.pathname == "/services" ? "active" : ""}`}>
                          <Link className="nav-link" href="services">Services</Link>
                      </li>
                     
                  </ul>
                  <div className='mobile-nav-btn'>
                      <i className='fa fa-times mobile-nav-icon close-outline' onClick={() => setMenuIcon(false)} />
                      <i className='fa fa-bars mobile-nav-icon' onClick={() => setMenuIcon(true)} />
                  </div>
              </div>
          </nav>  
          <style jsx>
          {`
           .mobile-nav-btn{
            display:none;
        }
        .close-outline{
            display:none;
          }
          .nav-item{
             padding: 3px;
          }
          .slick-nav{
            background-color:#25408f;
            box-shadow: 0 5px 15px rgba(0,0,0,0.1);
            border-top: 4px solid #3a4f70;
            height:70px;
            transition: all 0.5s ease;
          }
        .navbar{
            padding: 0px 0px;
            transition: all 0.5s ease;
            height:70px;
           }
        .navbar-brand{
            font-size:20px!important;
        }
        .navbar .nav-item .nav-link,.navbar-brand{
            color: #fff;
            text-transform: uppercase;
            font-size: 14px;
            font-weight: 400;
            position: relative;
        }
        .navbar .nav-item .nav-link::before{
            content: '';
            position: absolute;
            left: 0;
            bottom: 0;
            right: 0;
            height: 1px;
            background-color:#fff;
            transition: all 0.5s ease;
            transform: scale(0);
        }
        .navbar .nav-item .nav-link:hover::before{
            transform: scale(1);
        }
        
        .navbar .nav-item .nav-link.active::before,
        .navbar .nav-item .nav-link.active:hover::before{
            transform: scale(1);
        }
        
        @media (max-width:768px) {
            .mobile-nav{
                background-color:#25408f;
            }
            .mobile-nav-icon {
                position: relative;
                right: 10px;
                font-size: 30px;
            }
            .mobile-nav-btn{
                display:block;
                color:#fff;
            }
            .mobile-nav .mobile-nav-icon{
                display:none;
            }
            .mobile-nav .close-outline{
                display:block;
            }
            .navbar .navbar-nav{
                position: fixed;
                top: 3.3rem;
                right: 0;
                width: 0;
                background-color:#25408f;
                height: 100vh;
                transition: all 1s ease;
              }
              .navbar.mobile-nav .navbar-nav{
                width: 60%;
                transition: all 1s linear;
            }
            .navbar .nav-item .nav-link{
                text-align:center;
            }
           
        }
          `}
          </style>
    </>
  )
}

export default WebNav
