import AdminHeader from "../common/AdminHeader"
import AdminSidebar from "../common/AdminSidebar"

function Admin({children}) {
  return (
    <>
    <AdminHeader/>
    <AdminSidebar/>
    <main>{children}</main>
    </>
  )
}

export default Admin
