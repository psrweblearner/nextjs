import React from 'react'
import Website from './components/layouts/Website'
import Banner from './components/website/Banner'

const Home = () => {
  return (
    <>
      <Banner/>
    </>
  )
}

export default Home
Home.getLayout = function getLayout(page) {
  return (
   <Website>{page}</Website>
  )
}