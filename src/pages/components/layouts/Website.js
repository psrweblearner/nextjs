import Head from "next/head"
import { useRouter } from "next/router";
import WebFooter from "../common/WebFooter"
import WebNav from "../common/WebNav"

function Website({children}) {
  const router = useRouter();
  return (
    <>
    <Head>
      <title>Next Js Project {router.route}</title>
      <link rel="canonical" href={router.route}/>
    </Head>
    <WebNav/>
      <main>{children}</main>
    <WebFooter/>
    </>
  )
}

export default Website
