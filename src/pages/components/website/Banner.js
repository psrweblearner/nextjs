import React from 'react'

const Banner = () => {
  return (
    <>
       <div className='banner'>
        <div className='banner-content'>
            
        </div>
        <div className='banner-imgs'>
          {/* <img src="banner_image.jpg" alt="" className='img-fluid'/> */}
        <video autoPlay muted control='true' loop>
            <source src="/dubai1.mp4" type="video/mp4"/>
        </video>
        </div>
      </div>
      <style jsx>
      {`
      .banner-imgs img{
        width:100%;
        height:100vh;
      }
      .banner-imgs video{
        min-width: 100%;
        min-height:100%;
        width:100%;
      }
      @media (max-width:768px) {
        .banner-imgs img{
          width:100%;
          height:45vh;
        }
      }
      `}
      </style>
    </>
  )
}

export default Banner
